import com.google.common.collect.*;

public class Main {
    public static void main(String args[]) {
        Multimap map = createMap();
        Multimap newmap = swappingMap(map);
        System.out.println("Исходный Map: " + map);
        System.out.println("Измененный Map: " + newmap);
    }

    private static Multimap createMap() {
        Multimap<String, Integer> map = HashMultimap.create();
        map.put("key", 1);
        map.put("bruh", 1);
        map.put("dungeon", 2);
        map.put("dungeon", 3);
        return map;
    }

    private static Multimap swappingMap(Multimap map) {
        return Multimaps.invertFrom(map, HashMultimap.<Integer, String> create());
    }
}
